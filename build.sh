#!/usr/bin/env bash
DIR="`dirname $0`"

$DIR/foreman-proxy/build.sh
$DIR/tftpd/build.sh
$DIR/dhcpd/build.sh
$DIR/puppet/build.sh
