#!/usr/bin/env bash
cd "`dirname $0`"

FOREMAN_PROXY_IMAGE_VERSION="1"
FOREMAN_PROXY_IMAGE_TAG="${FOREMAN_PROXY_IMAGE_VERSION}"
FOREMAN_PROXY_IMAGE="docker.cloud.switch.ch:5000/foreman-proxy-foreman:${FOREMAN_PROXY_IMAGE_TAG}"

docker pull ubuntu:18.04
docker build -t ${FOREMAN_PROXY_IMAGE} . 
docker push ${FOREMAN_PROXY_IMAGE}
