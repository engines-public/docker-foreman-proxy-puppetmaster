# Build

    ./build.sh

or to build a single image

    cd [dhcpd|foreman-proxy|tftpd|puppet]
    ./build.sh


# Depoly dhcpd, foreman-proxy, tftpd

clone repo engines-bootstrap then run:

    ansible-playbook -i inventory/zhdk --limit <switch_name> playbooks/networkingfabric.yaml -t foreman-proxy

# Deploy puppet-master

on pm.<zhdk|unil>.cloud.switch.ch:

```bash
ip -6 neigh add proxy 2001:620:5ca1:8100:d0c::20 dev eth1
docker run -d --name pm-mitaka.s2.scloud.switch.ch \
           --env HOSTNAME=pm-mitaka.s2.scloud.switch.ch \
           --env PUPPET_VERSION=3.8 \
           --env FOREMAN_SERVER_URL="https://foreman.scloud.switch.ch" \
           --env CA_SERVER=foreman.scloud.switch.ch \
           -v /opt/mitaka/puppet-config/hiera:/etc/puppet/hiera/development_mitaka \
           -v /opt/mitaka/puppet-config/hiera.yaml:/etc/puppet/hiera.yaml \
           -v /opt/mitaka/development_mitaka:/etc/puppet/environments/development_mitaka \
           -v /opt/mitaka/ssl:/var/lib/puppet/ssl \
           -v /opt/mitaka/log:/var/log \
           --network dockerv6 --ip6 2001:620:5ca1:8100:d0c::20 \
           -ti foremanproxy
```
