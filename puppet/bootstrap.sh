#! /usr/bin/env bash

set -xeuo pipefail

sed -i -e "s#%HOSTNAME%#${HOSTNAME}#" \
    -e "s#%CA_SERVER%#${CA_SERVER}#" \
    "/etc/puppet/puppet.conf"

sed -i -e "s#%HOSTNAME%#${HOSTNAME}#" \
    "/etc/foreman-proxy/settings.d/puppet_proxy_legacy.yml"

sed -i -e "s#%PUPPET_VERSION%#${PUPPET_VERSION:-"3.8"}#" \
    "/etc/foreman-proxy/settings.d/puppet.yml"

sed -i -e "s#%HOSTNAME%#${HOSTNAME}#" \
    -e "s#%FOREMAN_SERVER_URL%#${FOREMAN_SERVER_URL}#" \
    "/usr/lib/ruby/vendor_ruby/puppet/reports/foreman.rb"

sed -i -e "s#%HOSTNAME%#${HOSTNAME}#" \
    -e "s#%FOREMAN_SERVER_URL%#${FOREMAN_SERVER_URL}#" \
    "/etc/puppet/node.rb"

sed -i -e "s#%HOSTNAME%#${HOSTNAME}#" \
    -e "s#%FOREMAN_SERVER_URL%#${FOREMAN_SERVER_URL}#" \
    "/etc/foreman-proxy/settings.yml"

trap stop INT TERM

start() {
    echo "Starting server..."
    if [ ! -f //var/lib/puppet/ssl/certs/${HOSTNAME}.pem ]; then
       puppet agent -t || true
    fi
    puppet master --verbose 
    /usr/share/foreman-proxy/bin/smart-proxy
}
stop() {
    echo "Stopping server..."
    exit
}
trap stop INT TERM
start
sleep infinity
stop

