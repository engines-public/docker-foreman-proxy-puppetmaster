#!/usr/bin/env bash
cd "`dirname $0`"


PUPPET_IMAGE_TAG=`date +\%Y\%m\%d`
PUPPET_IMAGE="docker.cloud.switch.ch:5000/foreman-proxy-puppet:${PUPPET_IMAGE_TAG}"

docker pull ubuntu:16.04
docker build -t ${PUPPET_IMAGE} . 
docker push ${PUPPET_IMAGE}
