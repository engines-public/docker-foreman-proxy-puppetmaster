#!/bin/bash

mkdir -p /var/lib/dhcp
touch /var/lib/dhcp/dhcpd.leases
exec /usr/sbin/dhcpd -f -d --no-pid
