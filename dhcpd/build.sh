#!/usr/bin/env bash
cd "`dirname $0`"

DHCPD_IMAGE_VERSION="1"
DHCPD_IMAGE_TAG="${DHCPD_IMAGE_VERSION}"
DHCPD_IMAGE="docker.cloud.switch.ch:5000/foreman-proxy-dhcpd:${DHCPD_IMAGE_TAG}"

docker pull ubuntu:18.04
docker build -t ${DHCPD_IMAGE} . 
docker push ${DHCPD_IMAGE}
