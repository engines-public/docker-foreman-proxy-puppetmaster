#!/usr/bin/env bash
cd "`dirname $0`"

TFTPD_IMAGE_VERSION="1"
TFTPD_IMAGE_TAG="${TFTPD_IMAGE_VERSION}"
TFTPD_IMAGE="docker.cloud.switch.ch:5000/foreman-proxy-tftpd:${TFTPD_IMAGE_TAG}"

docker pull ubuntu:18.04
docker build -t ${TFTPD_IMAGE} . 
docker push ${TFTPD_IMAGE}
