#!/bin/bash

if [ ! -d /srv/tftp/boot ]; then
    cd /
    tar -cf - tftp | tar -xf - -C /srv
fi

exec /usr/sbin/in.tftpd --listen --user tftp --address 0.0.0.0:69 --secure /srv/tftp --foreground
