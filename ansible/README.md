# foreman-proxy-ansible

Docker container for Foreman smart-proxy Ansible

## foreman-proxy-ansible container configuration

### /etc/foreman-proxy directory

The `/etc/foreman-proxy` directory must be mounted as volume.

If the `/etc/foreman-proxy/ssh/id_rsa` file exists, the SSH key will be copied in the `/usr/share/foreman-proxy/.ssh` directory, and used by the Foreman smart-proxy-ansible Ansible user.

### /etc/ansible directory

The `/etc/ansible` directory must be mounted as volume.

## Deployment

Deployment the container with Ansible. The Ansible playbook, and role is in our GitLab [`engines-bootstrap`](https://gitlab.cloud.switch.ch/petasolutions-switch-ch/engines-bootstrap) repository.

    ansible-playbook -i inventory/s2 playbooks/foreman-proxy-ansible.yml

## Starting the container by "hand"

Example for `ansible-proxy.s2.scloud.switch.ch` running on `pm.s2.scloud.switch.ch`

Prepare the required directories, and content to be mounted:

* `/etc/foreman-proxy-ansible` mounted as `/etc/foreman-proxy`
* `/etc/ansible`  mounted as `/etc/ansible`

Then use the following script to start the container:

```
#!/bin/bash
set -x
HOSTNAME="ansible-proxy.s2.scloud.switch.ch"
HOST_IPV6="2001:620:5ca1:8100:d0c::40"
DOCKER_IMAGE="docker.cloud.switch.ch:5000/foreman-proxy-ansible:1.15"

# create IPv6 address for the container
ip -6 neighbor add proxy $HOST_IPV6 dev eth1

docker run \
  -d \
  --restart=unless-stopped \
  --name ${HOSTNAME} \
  --net dockerv6 --ip6 ${HOST_IPV6} \
  --volume /etc/ansible:/etc/ansible \
  --volume /etc/foreman-proxy-ansible:/etc/foreman-proxy \
  -ti \
  ${DOCKER_IMAGE}
```
