#!/bin/bash

# Provision ssh key for Ansible
# if the /etc/foreman-proxy/ssh/id_rsa ssh key exists
# then copy it to the /usr/share/foreman-proxy/.ssh for
# the Ansible user
if [ -f "/etc/foreman-proxy/ssh/id_rsa" ] ; then
  echo "Provisioning Foreman smart-proxy-ansible Ansible ssh key..."
  mkdir -p /usr/share/foreman-proxy/.ssh
  cp /etc/foreman-proxy/ssh/id_rsa /usr/share/foreman-proxy/.ssh/id_rsa
  chmod 0400 /usr/share/foreman-proxy/.ssh/id_rsa
  chown -R foreman-proxy:foreman-proxy /usr/share/foreman-proxy/.ssh/
fi

echo "Start Foreman smart-proxy-ansible..."
exec /usr/share/foreman-proxy/bin/smart-proxy
